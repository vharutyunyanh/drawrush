using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DrawRush
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _holdToDrawText;
        [SerializeField] private List<Toggle> _toggles;
        [SerializeField] private Drawer _drawer;
        private TouchManager _touchManager;
        private bool _isPaused;

        private void Awake()
        {
            _touchManager = new TouchManager();
            _drawer.enabled = false;
            ResetToggles();
            _drawer.Completed += isSuccess =>
            {
                _toggles.First().isOn = isSuccess;
                _drawer.enabled = false;
                _isPaused = true;
                StartCoroutine(WaitForSeconds(5f));
            };
        }

        private IEnumerator WaitForSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            ResetToggles();

            _isPaused = false;
            _holdToDrawText.gameObject.SetActive(true);
            _drawer.ResetDrawer();
        }

        private void ResetToggles()
        {
            foreach (var toggle in _toggles)
            {
                toggle.isOn = false;
            }
        }

        private void Update()
        {
            if (!_isPaused)
            {
                if (!_drawer.enabled && _touchManager.GetTouches().Any())
                {
                    _drawer.enabled = true;
                    _holdToDrawText.gameObject.SetActive(false);
                }
                else if (_drawer.enabled && !_touchManager.GetTouches().Any())
                {
                    _drawer.enabled = false;
                }
            }
        }
    }
}
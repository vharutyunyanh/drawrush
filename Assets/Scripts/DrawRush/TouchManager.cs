using System.Collections.Generic;
using UnityEngine;

namespace DrawRush
{
    public class TouchManager
    {
        private Vector2 _lastPosition;

        private Touch GenerateFakeTouchFromMouse(TouchPhase phase = TouchPhase.Began)
        {
            var mousePosition = Input.mousePosition;
            var fakeTouch = new Touch
            {
                fingerId = 0,
                type = TouchType.Direct,
                radius = 1,
                phase = phase,
                position = mousePosition,
                deltaPosition = (Vector2) mousePosition - _lastPosition,
            };
            _lastPosition = mousePosition;
            return fakeTouch;
        }

        public Touch[] GetTouches()
        {
            var touches = new List<Touch>();
#if !UNITY_EDITOR
			for (int i = 0; i < Input.touchCount; ++i)
			{
				touches.Add(Input.GetTouch(i));
			}				
            return touches.ToArray();
#else
            touches.Clear();

            if (Input.GetMouseButtonDown(0))
            {
                _lastPosition = Input.mousePosition;
                touches.Add(GenerateFakeTouchFromMouse());
            }
            else if (Input.GetMouseButtonUp(0))
            {
                touches.Add(GenerateFakeTouchFromMouse(TouchPhase.Ended));
            }
            else if (Input.GetMouseButton(0))
            {
                touches.Add(GenerateFakeTouchFromMouse(TouchPhase.Moved));
            }

#endif


            return touches.ToArray();
        }
    }
}
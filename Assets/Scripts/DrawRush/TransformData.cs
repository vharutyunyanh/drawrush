using UnityEngine;

namespace DrawRush
{
    public class TransformData
    {
        public Vector3 Position { get; }
        public Vector3 Rotation { get; }
        public Vector3 LocalScale { get; }

        public TransformData(Transform original)
        {
            Position = original.position;
            Rotation = original.eulerAngles;
            LocalScale = original.localScale;
        }

        public void Apply(Transform transform)
        {
            transform.position = Position;
            transform.eulerAngles = Rotation;
            transform.localScale = LocalScale;
        }
    }
}
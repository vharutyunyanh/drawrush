using UnityEngine;

namespace DrawRush
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class PenController : MonoBehaviour
    {
        [SerializeField] private Transform _penTransform;
        private Rigidbody _rigidBody;
        private Collider _collider;
        private Vector3 _lastPosition;
        private TransformData _initialTransformData;
        private float _initialDrag;


        private void Awake()
        {
            _rigidBody = GetComponentInChildren<Rigidbody>();
            _collider = GetComponentInChildren<Collider>();
            _initialTransformData = new TransformData(_penTransform);
            _initialDrag = _rigidBody.drag;
            ResetPen();
        }

        public void ResetPen()
        {
            _rigidBody.isKinematic = true;
            _rigidBody.velocity = Vector3.zero;
            _rigidBody.angularVelocity = Vector3.zero;
            _initialTransformData.Apply(_penTransform);
            _rigidBody.drag = _initialDrag;
            _rigidBody.useGravity = false;
            SetPenPosition(false);
        }

        public void StartDrawing()
        {
            enabled = true;
            SetPenPosition(true);
        }

        public void StopDrawing()
        {
            enabled = false;
            SetPenPosition(false);
        }

        private void FixedUpdate()
        {
            var translation = _penTransform.position - _lastPosition;
            var velocity = translation / Time.fixedDeltaTime;
            var transformEulerAngles = _penTransform.eulerAngles;
            _penTransform.eulerAngles = new Vector3(transformEulerAngles.x, velocity.x / 5f, transformEulerAngles.z);

            _lastPosition = transform.position;
        }

        private void SetPenPosition(bool isEnabled)
        {
            var initialZ = _initialTransformData.Position.z;
            var penPosition = _penTransform.position;
            _penTransform.position = new Vector3(penPosition.x, penPosition.y, isEnabled ? initialZ : initialZ - 1f);
        }

        public void Drop()
        {
            enabled = false;
            _rigidBody.useGravity = true;
            _rigidBody.isKinematic = false;
            _rigidBody.drag = 2f;
        }
    }
}
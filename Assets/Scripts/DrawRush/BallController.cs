using System;
using UnityEngine;

namespace DrawRush
{
    [RequireComponent(typeof(Rigidbody))]
    public class BallController : MonoBehaviour
    {
        [SerializeField] private Collider _penCollider;
        private Rigidbody _rigidBody;
        private TransformData _initialTransformData;
        private float _initialDrag;

        public Action OnCollidedWithPen { get; set; }

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody>();
            _initialTransformData = new TransformData(transform);
            _initialDrag = _rigidBody.drag;
        }

        public void ResetBall()
        {
            _rigidBody.velocity = Vector3.zero;
            _rigidBody.angularVelocity = Vector3.zero;
            _initialTransformData.Apply(transform);
            _rigidBody.drag = _initialDrag;
            var x = 3f;
            var y = 3f;
            _rigidBody.AddForce(new Vector2(x, y), ForceMode.VelocityChange);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider == _penCollider)
            {
                OnCollidedWithPen?.Invoke();
            }
        }

        public void Drop()
        {
            _rigidBody.velocity = Vector3.zero;
            _rigidBody.angularVelocity = Vector3.zero;
            _rigidBody.drag = 2f;
            var direction = _penCollider.transform.position - transform.position;
            direction.Normalize();
            direction *= 30;
            _rigidBody.AddForce(new Vector2(direction.x, direction.y), ForceMode.Impulse);
        }
    }
}
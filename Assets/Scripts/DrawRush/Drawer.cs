﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DrawRush
{
    public class Drawer : MonoBehaviour
    {
        [SerializeField] private Transform _maskPrefab;
        [SerializeField] private Collider2D _completeCollider;
        [SerializeField] private PenController _pen;
        [SerializeField] private BallController _ball;

        private Transform _currentMask;
        private readonly Queue<GameObject> _generatedMasks = new Queue<GameObject>();
        private const float MaxSpeed = 40f;
        private int _direction;
        private float _xSpeed;
        private Vector3 _initialPosition;
        private const float InitialXSpeed = 5f;
        private const float YSpeed = 1.5f;

        public Action<bool> Completed { get; set; }

        private void Awake()
        {
            _initialPosition = transform.position;
            _ball.OnCollidedWithPen += OnCollidedWithPen;
        }

        private void Start()
        {
            ResetDrawer();
        }

        private void OnCollidedWithPen()
        {
            _pen.Drop();
            _ball.Drop();
            Completed?.Invoke(false);
        }

        private void OnEnable()
        {
            _pen.StartDrawing();
        }

        private void OnDisable()
        {
            _pen.StopDrawing();
            ResetXsPeed();
        }

        public void ResetDrawer()
        {
            _direction = 1;
            ResetXsPeed();
            transform.position = _initialPosition;
            while (_generatedMasks.Any())
            {
                Destroy(_generatedMasks.Dequeue());
            }

            _pen.ResetPen();
            _ball.ResetBall();
            CreateMask();
            UpdateCurrentMask();
        }

        private void ResetXsPeed()
        {
            _xSpeed = InitialXSpeed;
        }

        private void FixedUpdate()
        {
            transform.Translate(GetDisplacement(), Space.Self);
            UpdateCurrentMask();
        }

        private void UpdateCurrentMask()
        {
            var displacement = GetDisplacement();
            var angle = Mathf.Atan2(displacement.y, displacement.x);
            _currentMask.eulerAngles = Vector3.forward * (_direction > 0 ? angle : 180 - angle);
            _currentMask.position = transform.position;
        }

        private Vector2 GetDisplacement()
        {
            var deltaTime = Time.deltaTime;

            var xShift = deltaTime * _direction * _xSpeed;
            var yShift = deltaTime * YSpeed;
            return new Vector2(xShift, yShift);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            _direction *= -1;
            _xSpeed = Math.Min(_xSpeed * Mathf.Exp(_xSpeed / 10f), MaxSpeed);
            CreateMask();
            UpdateCurrentMask();
        }

        private void CreateMask()
        {
            _currentMask = Instantiate(_maskPrefab);
            _generatedMasks.Enqueue(_currentMask.gameObject);
            if (_generatedMasks.Count > 4)
            {
                Destroy(_generatedMasks.Dequeue());
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
//            Debug.LogError("OnTriggerEnter2D");
            if (other == _completeCollider)
            {
                Completed?.Invoke(true);
                Debug.LogError("Completed");
            }
        }

        private void OnDestroy()
        {
            _ball.OnCollidedWithPen -= OnCollidedWithPen;
        }
    }
}